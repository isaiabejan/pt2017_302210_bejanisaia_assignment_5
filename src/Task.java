import org.joda.time.DateTime;

import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class Task {

    public static void task1(List<MonitoredData> monitoredData, PrintWriter writer) {
        writer.println("Task 1: ");
        writer.println(monitoredData.stream().collect(Collectors.groupingBy(md -> md.getEndTime().getDayOfMonth())).keySet().size());
    }

    public static void task2(List<MonitoredData> monitoredData, PrintWriter writer) {
        writer.println("Task 2: ");
        writer.println(monitoredData.stream().collect(Collectors.groupingBy(md -> md.getActivityLabel(), Collectors.counting())));
    }

    public static void task3(List<MonitoredData> monitoredData, PrintWriter writer) {
        writer.println("Task 3: ");
        writer.println(monitoredData.stream().collect(Collectors.groupingBy(md -> md.getEndTime().getDayOfMonth(), Collectors.groupingBy(md -> md.getActivityLabel(), Collectors.counting()))));
    }

    public static void task4(List<MonitoredData> monitoredData, PrintWriter writer) {
        final Map<String, DateTime> result =
        monitoredData.stream()
                .collect(Collectors.groupingBy(md -> md.getActivityLabel(),
                        Collectors.summingLong(data -> data.getDuration().getStandardMinutes())))
                .entrySet().stream().filter(s -> s.getValue() > 600 || s.getValue() < 0)
                .collect(Collectors.toMap(s -> s.getKey(), s -> new DateTime(s.getValue())));
        writer.println("Task 4: ");
        writer.println(result);
    }

    public static void task5(List<MonitoredData> monitoredData, PrintWriter writer) {

        List<String> result = new ArrayList<String>();

        final Map<String, Long> total = monitoredData.stream()
                .collect(Collectors.groupingBy(md->md.getActivityLabel(), Collectors.counting()));

        final Map<String, Long> totalLess5 = monitoredData.stream()
                .filter(md -> md.getDuration().getStandardMinutes() < 5)
                .collect(Collectors.groupingBy(md -> md.getActivityLabel(), Collectors.counting()));

        total.keySet().forEach(key -> {
            Long totalNr = total.get(key);
            Long total5 = totalLess5.get(key);
            if(total5 == null) {
                total5 = 0L;
            }
            if(total5/totalNr >= 0.9) {
                result.add(key);
            }
        });

        writer.println("Task 5: ");
        writer.println(result);

    }
}
