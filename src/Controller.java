

public class Controller {
    MainClass mainClass = new MainClass();

    public void doTask1() {
        mainClass.doFirstTask();
    }

    public void doTask2() {
        mainClass.doSecondTask();
    }

    public void doTask3() {
        mainClass.doThirdTask();
    }

    public void doTask4() {
        mainClass.doFourthTask();
    }

    public void doTask5() {
        mainClass.doFifthTask();
    }
}

