import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class MainClass {
    private List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();
    private PrintWriter writer = null;

    public PrintWriter getWriter() {
        return writer;
    }

    public List<MonitoredData> getMonitoredData() {
        return monitoredData;
    }

    public MainClass() {

        try (BufferedReader br = new BufferedReader(new FileReader("activities.txt"))) {
            String line = br.readLine();
            while (line != null) {
                String[] tokens = line.split("\t\t");
                DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-mm-dd HH:mm:ss");
                monitoredData.add(new MonitoredData(format.parseDateTime(tokens[0]), format.parseDateTime(tokens[1]), tokens[2]));
                line = br.readLine();
            }
            br.close();
            writer = new PrintWriter("results.txt");
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }


    public void doFirstTask() {
        Task.task1(this.getMonitoredData(), this.getWriter());
        this.getWriter().flush();
    }

    public void doSecondTask() {
        Task.task2(this.getMonitoredData(), this.getWriter());
        this.getWriter().flush();
    }

    public void doThirdTask() {
        Task.task3(this.getMonitoredData(), this.getWriter());
        this.getWriter().flush();
    }

    public void doFourthTask() {
        Task.task4(this.getMonitoredData(), this.getWriter());
        this.getWriter().flush();
    }

    public void doFifthTask() {
        Task.task5(this.getMonitoredData(), this.getWriter());
        this.getWriter().flush();
    }

//            writer.close();
//            br.close();
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
}
